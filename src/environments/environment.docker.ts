export const environment = {
  production: true,
  SERVER_URL: 'http://localhost:3000',
  VAPID_PUBLIC: 'BJQqQhdaxeEmFMIy_HUZ1EjXCHtZ-_LdE-WYiV7laVzXTyETchVGVJkrWEhFNoUWIow8OtLz1TL6fKNk_qpzbsM',
  firebaseConfig:  {
    projectId: "monitor-vacina-covid",
  },
  useFirestoreEmulator: true
};
