import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { SwPush } from '@angular/service-worker';
import { filter, map, switchMap } from 'rxjs/operators';
import { UserSubscription } from 'src/app/shared/models/userSubscription.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'tile-form',
  templateUrl: 'tile-form.component.html'
})

export class TileFormComponent implements OnInit {
  subscribeLoading = false;
  undoSubscribeLoading = false;

  permissaoNegada = false;

  doses = [
    {label: '1ª Dose', value: 1},
    {label: '2ª Dose', value: 3}
  ];

  tipos = [
    {label: 'acima de 60 anos', value: 0},
    {label: 'de 60 a 65 anos', value: 1},
    {label: 'acima de 65 anos', value: 2},
    {label: 'acima de 70 anos', value: 3},
    {label: 'acima de 75 anos', value: 4},
    {label: 'acima de 85 anos', value: 5},
    {label: 'Trabalhador da saúde', value: 6},
  ];

  userSubscription: UserSubscription = new UserSubscription();
  private userSubCollection!: AngularFirestoreCollection<UserSubscription>;

  constructor(
    private swPush: SwPush,
    private firestore: AngularFirestore
  ) {
    this.userSubCollection = this.firestore.collection<UserSubscription>('subscriptions');
  }

  ngOnInit(): void {

    this.swPush.subscription
      .pipe(
        map(sub => UserSubscription.getKeyFromSubscription(sub)),
        filter(key => key !== undefined),
        switchMap(key => this.userSubCollection.doc(key).valueChanges())
      ).subscribe({
        next: (userSub) => {
          this.userSubscription = new UserSubscription(userSub);
        },
        error: (error) => {
          console.log(error);
          this.userSubscription = new UserSubscription();
        }
      });
  }

  toggleArrayValue(value: any, array: any[]): void {
    const index = array.indexOf(value);
    if (index > - 1) {
      array.splice(index, 1);
    } else {
      array.push(value);
    }
  }

  toggleDose(value: number): void {
    this.toggleArrayValue(value, this.userSubscription.doses);
  }

  toggleGrupo(value: number): void {
    this.toggleArrayValue(value, this.userSubscription.tipos);
  }

  sendPushUnsubscribe(): void {
    if (this.swPush.isEnabled) {
      this.undoSubscribeLoading = true;
      this.userSubCollection.doc(this.userSubscription.key).delete()
        .then(() => {
          // Toast
        })
        .catch(error => {
          console.log(error);
        })
        .finally(() => {
          this.undoSubscribeLoading = false;
        });
    }
  }

  async sendPushSubscribe(): Promise<void> {
    if (this.userSubscription.doses.length > 0 && this.userSubscription.tipos.length > 0) {
      if (this.swPush.isEnabled) {
        this.subscribeLoading = true;

        if (!this.userSubscription.subscription) {
          try {
            this.userSubscription.subscription = await this.swPush.requestSubscription({serverPublicKey: environment.VAPID_PUBLIC});
          } catch (error) {
            this.subscribeLoading = false;

            this.permissaoNegada = true;
            return;
            // Toast
          }
        }

        if (this.userSubscription.subscription == null) {
          this.permissaoNegada = true;
          return;
        }

        this.userSubCollection
            .doc(this.userSubscription.key)
            .set(this.userSubscription.toSend(), {merge: true})
            .then(() => {
              // Toast
            })
            .catch(error => {
              // Toast
              console.error(error);
            })
            .finally(() => this.subscribeLoading = false);
      }
    }
  }
}
