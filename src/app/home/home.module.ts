import { FireModule } from './../shared/fire.module';
import { TileFormComponent } from './components/tile-form/tile-form.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TileInfoComponent } from './components/tile-info/tile-info.component';

import { HomeComponent } from './page/home.component';
import { ReactiveFormsModule } from '@angular/forms';

const ROUTES: Routes = [
  { path: '', component: HomeComponent }
]

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule,
    SharedModule,
    FireModule
  ],
  exports: [],
  declarations: [HomeComponent, TileInfoComponent, TileFormComponent],
  providers: [],
})
export class HomeModule { }
