import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: 'checkbox.component.html'
})

export class CheckBoxComponent implements OnInit {
  @Input() value: any;
  @Input() label!: string;
  @Input() size: 'is-small' | 'is-medium' | 'is-large' = 'is-small';
  @Input() checked: boolean = false;

  constructor() { }

  ngOnInit() { }
}
