import { Dose, Tipo } from "../interfaces/grupo.interface";

export class UserSubscription {
  public key?: string;
  public doses: Dose[];
  public tipos: Tipo[];

  #subscription: PushSubscription | PushSubscriptionJSON | null;
  set subscription(sub: PushSubscription | PushSubscriptionJSON | null) {
    this.#subscription = sub;
    this.key = UserSubscription.getKeyFromSubscription(sub);
  }
  get subscription(): PushSubscription | PushSubscriptionJSON | null {
    return this.#subscription;
  }

  constructor(userSub?: UserSubscription) {
    this.doses = userSub?.doses || [];
    this.tipos = userSub?.tipos || [];
    this.#subscription = userSub?.subscription ||  null;
    this.key = UserSubscription.getKeyFromSubscription(this.subscription);
  }

  static getKeyFromSubscription(sub: PushSubscription | PushSubscriptionJSON | null): string | undefined {
    if (!sub) { return undefined; }
    else if (sub instanceof PushSubscription) { return `${sub.toJSON().keys?.p256dh}${sub.toJSON().keys?.auth}`; }
    else { return `${sub.keys?.p256dh}${sub.keys?.auth}`; }
  }

  public toSend(): any {
    return {
      doses: this.doses,
      tipos: this.tipos,
      subscription: this.#subscription instanceof PushSubscription ? this.#subscription?.toJSON() : this.#subscription
    };
  }
}
