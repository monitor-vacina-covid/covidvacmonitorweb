import { LocalGrupo } from './../interfaces/localGrupo.interface';
import { Retorno } from './../interfaces/retorno.interface';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSubscription } from '../models/userSubscription.model';

@Injectable({providedIn: 'root'})
export class CovidVacMonitorService {
  constructor(
    private http: HttpClient
  ) { }

  public disponiveis() {
    return this.http.get<Retorno<LocalGrupo[]>>(environment.SERVER_URL);
  }

}
