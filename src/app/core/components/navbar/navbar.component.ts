import { ViewChild, Component, AfterViewInit, ElementRef } from '@angular/core';
import { createPopper, Instance } from '@popperjs/core';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements AfterViewInit {
    @ViewChild('agendamentoIcon') agendamentoIcon!: ElementRef;
    @ViewChild('agendamentoTooltip') agendamentoTooltip!: ElementRef;

    constructor(
    ) {}

    ngAfterViewInit(): void {
      const popperInstance = createPopper(this.agendamentoIcon.nativeElement, this.agendamentoTooltip.nativeElement, {
        placement: 'bottom-start',
        modifiers: [
          {
            name: 'offset',
            options: {
              offset: [0, 8]
            }
          }
        ]
      })

      setTimeout(() => {
        popperInstance.destroy();
        this.agendamentoTooltip.nativeElement.remove();
      }, 5000);
    }
}
